package com.example

import com.example.com.configureHTTP
import com.example.com.configureRouting
import com.example.com.configureSerialization
import com.route.TodoListRouting
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*

fun main() {
    embeddedServer(Netty, port = 8000, host = "localhost") {
        configureSerialization()
        routing {
            TodoListRouting()
        }
    }.start(wait = true)
}
