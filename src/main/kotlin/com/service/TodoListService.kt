package com.service

import com.model.*

class TodoListService : TodoListApi {
    var TodoList = listOf<TodoItem>(
        TodoItem(
            ItemId(0),
            ItemContents("aa"),
            ItemaCompletedStatus(true)
        ),
        TodoItem(
            ItemId(1),
            ItemContents("bb"),
            ItemaCompletedStatus(true)
        ),
        TodoItem(
            ItemId(2),
            ItemContents("cc"),
            ItemaCompletedStatus(false)
        ),
        TodoItem(
            ItemId(3),
            ItemContents("dd"),
            ItemaCompletedStatus(false)
        )
    )

    override fun getAllTodoList(): Pair<List<Pair<String, ItemaCompletedStatus>>, List<Pair<String, ItemaCompletedStatus>>> = TodoList.map { Pair(it.contents.value,it.status) }.partition { it.second.value }

    override fun getActiveTodoList(): List<String> = TodoList.filter { !it.status.value }.map { it.contents.value }

    override fun getCompletedTodoList(): List<String> = TodoList.filter { it.status.value }.map { it.contents.value }

    override fun addTodo(title: String): Unit {
        TODO()
    }

    override fun updateTodo(title: String): Boolean {
        TODO()
    }

    override fun deleteTodo(id: Int): Unit {
        TODO()
    }

}