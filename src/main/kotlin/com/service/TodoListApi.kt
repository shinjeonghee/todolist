package com.service

import com.model.ItemaCompletedStatus
import com.model.TodoItem


interface TodoListApi {

    fun getAllTodoList(): Pair<List<Pair<String, ItemaCompletedStatus>>, List<Pair<String, ItemaCompletedStatus>>>
    fun getActiveTodoList(): List<String>
    fun getCompletedTodoList(): List<String>
    fun addTodo(TodoListContent: String): Unit
    fun updateTodo(TodoListContent: String): Boolean
    fun deleteTodo(TodoListId: Int): Unit
}