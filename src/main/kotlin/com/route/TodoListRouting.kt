package com.route

import com.service.TodoListService
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*

fun Route.TodoListRouting() {

    var Todoservice = TodoListService()

    get("/") {
        call.respond(Todoservice.getAllTodoList())
    }
}
