package com.model

@JvmInline
value class ItemId(val value :Int)
@JvmInline
value class ItemContents(val value : String)
@JvmInline
value class ItemaCompletedStatus(val value : Boolean = false)

data class TodoItem (
    val id: ItemId,
    val contents: ItemContents,
    val status: ItemaCompletedStatus
)